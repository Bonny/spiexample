package com.spi.example.iproxy.impl;

import com.spi.example.iproxy.IBaseProxy;

public class Proxy2 implements IBaseProxy {

	@Override
	public void execute() {
		System.out.println(String.format("%s execute()", this.getClass().getSimpleName()));
	}

}
