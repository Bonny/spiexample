package com.spi.example.iproxy.impl;

import com.spi.example.iproxy.IBaseProxy;

public class Proxy1 implements IBaseProxy {

	@Override
	public void execute() {
		System.out.println(String.format("%s execute()", this.getClass().getSimpleName()));
	}

}
