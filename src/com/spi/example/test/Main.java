package com.spi.example.test;

import java.util.ServiceLoader;

import com.spi.example.aproxy.ABaseProxy;
import com.spi.example.iproxy.IBaseProxy;

public class Main {

	public static void main(String[] args) {

		System.out.println("START main");

		ServiceLoader<IBaseProxy> services = ServiceLoader.load(IBaseProxy.class);

		for (IBaseProxy i : services)
			i.execute();

		ServiceLoader.load(ABaseProxy.class).forEach(i -> {
			System.out.println(i.getClass());
			System.out.println(String.format("%s execute(2) -> %d", i.getClass().getSimpleName(), i.execute(2)));
		});

		System.out.println("END main");

	}

}
