package com.spi.example.aproxy;

public abstract class ABaseProxy {

	public abstract int execute(int input);
	
}
