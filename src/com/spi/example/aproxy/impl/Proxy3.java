package com.spi.example.aproxy.impl;

import com.spi.example.aproxy.ABaseProxy;

public class Proxy3 extends ABaseProxy {

	@Override
	public int execute(int input) {
		return input * 4;
	}

}
